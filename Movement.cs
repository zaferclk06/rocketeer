using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] float mainthrust=0f;
    [SerializeField] float rotationthrust=0f;
    Rigidbody rb;
        void Start()
    {
        rb=GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRotation();
    }
    void ProcessThrust()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            
            rb.AddRelativeForce(Vector3.up*Time.deltaTime*mainthrust);
        }
        


    }
    void ProcessRotation()
    {
        if (Input.GetKey(KeyCode.A))
        {
            ApplyRotation(rotationthrust);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            ApplyRotation(-rotationthrust);
        }

    }

    void ApplyRotation(float rotationthisframe)
    {
        rb.freezeRotation=true; // freezing rotation so we can manually rotate
        transform.Rotate(Vector3.forward * Time.deltaTime * rotationthisframe);
        rb.freezeRotation=false;// unfreezing rotation so physics system can take over
        
    }
}
